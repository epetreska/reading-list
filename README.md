## Reading Challenge: 
- Re-read a book: The Subtle Art (Mark Manson)
- Banned Book: Tropic of Cancer (Henry Miller)
- Fiction book: Shantaram (Gregory Roberts)
- Review a book like a critic: Homo Deus (Yuval Harari) 
- 5 books recommended: 
    - Animal Farm (George Orwell)
    - The Millionaire Fast Lane (MJ DeMarco)
    - Man's Search for Meaning (Victor Frankl)
    - The Road Less Traveled (Scott Peck)
    - 12 Rules for Life (Jordan Peterson)
    
## Reading list:
- Hiking with Nietzsche (John Kaag)
- Medidations (Marcus Aurelius)
- Confessions of a Yakuza (Junichi Saga)
- Dune (Frank Herbert)
- The Art of War (Sun Tzu)
- Tao Te Ching (Lao Tzu)
- Panchiko (Min Jin Lee)
- War & Peace (Tolstoy)
- The Book of 5 Rings (Miyamoto Misashi)
- Buddha (Karen Armstrong)
- What If (Randall Munroe)
- Foucault's Pendulum (Umberto Eco)
- The Chomsky- Foucault Debate 
- The Money Tree (Chris Guillebeau)
- The Holy Sh*t Moment (James Fell)
- Atomic Habits (James Clear)
- The Power (Alderman Novel)
- Influence (Robert Cialdini)
- High Output Management (Andrew Grove)
- Breathe (James Nestor)
- That which is seen, and that which is not seen (Frederic Bastiat)

## Survival courses:
- https://www.udemy.com/course/survival/
- https://www.udemy.com/course/tsd-emergencies-course/
- https://www.realisticpreparedness.com/online-survival-skills-training-realistic-preparedness-survival-school/

## Survival books:
- Be Ready When The Sh*t Goes Down (Forest Griffin/Eric Krauss)
- 98.6 Degrees (Cody Lundin)
- When All Hell Breaks Loose (Cody Lundin)

